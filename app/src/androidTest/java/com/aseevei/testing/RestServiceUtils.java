package com.aseevei.testing;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class RestServiceUtils {

  private RestServiceUtils() {
  }

  public static String getStringFromFile(Context context, String filePath) throws Exception {
    final InputStream stream = context.getResources().getAssets().open(filePath);
    String content = convertStreamToString(stream);
    stream.close();
    return content;
  }

  private static String convertStreamToString(InputStream inputStream) throws Exception {
    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
    StringBuilder stringBuilder = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      stringBuilder.append(line).append("\n");
    }
    reader.close();
    return stringBuilder.toString();
  }
}
