package com.aseevei.teamworkprojects.injection;

import android.app.Application;
import com.aseevei.teamworkprojects.ui.project.list.ProjectListPresenterHolder;
import dagger.BindsInstance;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Application component that provides all common components.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void injectListHolder(ProjectListPresenterHolder viewModel);
}
