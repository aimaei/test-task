package com.aseevei.teamworkprojects;

import android.app.Application;
import com.aseevei.teamworkprojects.injection.AppComponent;
import com.aseevei.teamworkprojects.injection.DaggerAppComponent;

/** Represents an application context of the application. */
public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .build();
    }

    /**
     * Get application component instance.
     *
     * @return application component for providing dependencies.
     */
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
