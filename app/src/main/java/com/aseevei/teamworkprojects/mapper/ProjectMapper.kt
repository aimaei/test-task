package com.aseevei.teamworkprojects.mapper

import com.aseevei.teamworkprojects.model.ProjectUiModel
import com.aseevei.teamworkprojects.presentation.model.ProjectView
import javax.inject.Inject

/**
 * Map a [ProjectView] to and from a [ProjectUiModel] instance when data is moving between
 * this layer and the Domain layer.
 */
open class ProjectMapper @Inject constructor() : Mapper<ProjectUiModel, ProjectView> {

    /**
     * Map a [ProjectView] instance to a [ProjectUiModel] instance.
     */
    override fun mapToUIModel(type: ProjectView): ProjectUiModel {
        return ProjectUiModel(type.name, type.description, type.logo)
    }
}
