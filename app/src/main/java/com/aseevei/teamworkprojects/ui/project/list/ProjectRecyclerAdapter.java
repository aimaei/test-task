package com.aseevei.teamworkprojects.ui.project.list;

import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.aseevei.teamworkprojects.R;
import com.aseevei.teamworkprojects.model.ProjectUiModel;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that creates and binds views, that shows project detail. All methods must be called from main thread.
 */
public class ProjectRecyclerAdapter extends RecyclerView.Adapter {

    private List<ProjectUiModel> items = new ArrayList<>();
    private ItemClickListener itemClickListener;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProjectViewHolder(LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_project, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ProjectViewHolder holder = (ProjectViewHolder) viewHolder;
        ProjectUiModel project = items.get(position);
        holder.nameText.setText(project.getName());
        holder.descriptionText.setText(project.getDescription());
        Glide.with(holder.avatarImage)
                .load(project.getLogo())
                .apply(circleCropTransform().placeholder(R.drawable.project_placeholder))
                .into(holder.avatarImage);
        holder.itemView.setOnClickListener(ign -> {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(project);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Add project to current list. It will automatically notify adapter about data changes.
     */
    public void addProjects(@NonNull List<ProjectUiModel> newProjects) {
        items.addAll(newProjects);
        notifyItemRangeInserted(getItemCount() - newProjects.size(), newProjects.size());
    }

    /**
     * Register a callback to be invoked when the item is clicked.
     */
    public void setItemClickListener(@NonNull ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public static class ProjectViewHolder extends RecyclerView.ViewHolder {

        ImageView avatarImage;
        TextView nameText;
        TextView descriptionText;

        public ProjectViewHolder(@NonNull View itemView) {
            super(itemView);
            avatarImage = itemView.findViewById(R.id.image_avatar);
            nameText = itemView.findViewById(R.id.text_name);
            descriptionText = itemView.findViewById(R.id.text_description);
        }
    }

    public interface ItemClickListener {
        void onItemClick(ProjectUiModel project);
    }
}
