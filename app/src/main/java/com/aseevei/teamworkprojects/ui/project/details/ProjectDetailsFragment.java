package com.aseevei.teamworkprojects.ui.project.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.aseevei.teamworkprojects.R;
import com.aseevei.teamworkprojects.model.ProjectUiModel;
import com.bumptech.glide.Glide;

public class ProjectDetailsFragment extends Fragment {

    public static final String KEY_PROJECT = "KEY_PROJECT";

    //regionViews
    @BindView(R.id.image_avatar)
    ImageView avatarImage;

    @BindView(R.id.text_name)
    TextView nameText;

    @BindView(R.id.text_description)
    TextView descriptionText;
    //endregion

    private Unbinder unbinder;
    private ProjectUiModel project;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        project = (ProjectUiModel) getArguments().getSerializable(KEY_PROJECT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        Glide.with(requireContext())
                .load(project.getLogo())
                .into(avatarImage);
        nameText.setText(project.getName());
        descriptionText.setText(project.getDescription());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
