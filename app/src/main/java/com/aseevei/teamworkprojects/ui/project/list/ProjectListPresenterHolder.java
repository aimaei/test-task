package com.aseevei.teamworkprojects.ui.project.list;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import com.aseevei.teamworkprojects.App;
import com.aseevei.teamworkprojects.presentation.list.ListProjectPresenter;
import javax.inject.Inject;

/**
 * {@link ListProjectPresenter} instance holder for keeping it in memory while Activity/Fragment is rotating,
 * because {@link AndroidViewModel} will be killed only when Activity/Fragment is finishing.
 */
public class ProjectListPresenterHolder extends AndroidViewModel {

  @Inject
  ListProjectPresenter presenter;

  public ProjectListPresenterHolder(@NonNull Application application) {
    super(application);
    ((App) application).getAppComponent().injectListHolder(this);
  }

  public ListProjectPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    presenter.stop();
  }
}
