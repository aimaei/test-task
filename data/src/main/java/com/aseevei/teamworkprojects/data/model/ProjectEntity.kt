package com.aseevei.teamworkprojects.data.model

/**
 * Representation for a [ProjectEntity] fetched from an external layer data source
 */
data class ProjectEntity(val name: String, val description: String, val logo: String)
