package com.aseevei.teamworkprojects.cache;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.aseevei.teamworkprojects.cache.db.DbOpenHelper;
import com.aseevei.teamworkprojects.cache.mapper.ProjectEntityMapper;
import com.aseevei.teamworkprojects.cache.model.CachedProject;
import com.aseevei.teamworkprojects.data.model.ProjectEntity;
import com.aseevei.teamworkprojects.data.repository.ProjectCache;
import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class ProjectCacheImpl implements ProjectCache {

    private final static long MINUTE_IN_MILLIS = 60 * 1000;
    private final static long EXPIRATION_TIME = 10 * MINUTE_IN_MILLIS;

    private final DbOpenHelper dbOpenHelper;
    private final ProjectEntityMapper projectEntityMapper;
    private final PreferencesHelper preferencesHelper;

    @Inject
    public ProjectCacheImpl(final DbOpenHelper dbOpenHelper,
                            final ProjectEntityMapper projectEntityMapper,
                            final PreferencesHelper preferencesHelper) {
        this.dbOpenHelper = dbOpenHelper;
        this.projectEntityMapper = projectEntityMapper;
        this.preferencesHelper = preferencesHelper;
    }

    @Override
    public Completable clearProjects() {
        return Completable.create(emitter -> {
            SQLiteDatabase database = getDatabase();
            runInTransaction(database, () -> database.delete(DbOpenHelper.TABLE_NAME, null, null));
            emitter.onComplete();
        });
    }

    @Override
    public Completable saveProjects(List<ProjectEntity> projects) {
        return Completable.create(emitter -> {
            SQLiteDatabase database = getDatabase();
            runInTransaction(database, () -> {
                for (ProjectEntity project : projects) {
                    ContentValues values = new ContentValues();
                    values.put(DbOpenHelper.COLUMN_NAME, project.getName());
                    values.put(DbOpenHelper.COLUMN_DESCRIPTION, project.getDescription());
                    values.put(DbOpenHelper.COLUMN_LOGO, project.getLogo());
                    database.insert(DbOpenHelper.TABLE_NAME, null, values);
                }
            });
            emitter.onComplete();
        });
    }

    @Override
    public Single<List<ProjectEntity>> getProjects() {
        return Single.create(emitter -> {
            Cursor cursor = getDatabase().rawQuery("SELECT * FROM " + DbOpenHelper.TABLE_NAME, null);
            ArrayList<ProjectEntity> projects = new ArrayList<>();
            while (cursor.moveToNext()) {
                CachedProject cachedProject = new CachedProject(
                        cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.COLUMN_NAME)),
                        cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.COLUMN_DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.COLUMN_LOGO))
                );
                projects.add(projectEntityMapper.mapFromCached(cachedProject));
            }
            cursor.close();
            emitter.onSuccess(projects);
        });
    }

    @Override
    public boolean isCached() {
        Cursor cursor = getDatabase().rawQuery("SELECT * FROM " + DbOpenHelper.TABLE_NAME, null);
        boolean isCached = cursor.getCount() > 0;
        cursor.close();
        return isCached;
    }

    @Override
    public void setLastCacheTime(long lastCache) {
        preferencesHelper.setLastCacheTime(lastCache);
    }

    @Override
    public boolean isExpired() {
        return System.currentTimeMillis() - preferencesHelper.getLastCacheTime() > EXPIRATION_TIME;
    }

    public SQLiteDatabase getDatabase() {
        return dbOpenHelper.getWritableDatabase();
    }

    private static void runInTransaction(SQLiteDatabase database, Runnable runnable) {
        database.beginTransaction();
        try {
            runnable.run();
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }
}
