package com.aseevei.teamworkprojects.cache;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {

    private static final String PREFERENCES_NAME = "com.aseevei.teamworkprojects.cache";
    private static final String KEY_LAST_CACHE_TIME = "KEY_LAST_CACHE_TIME";
    private final SharedPreferences preferences;

    @Inject
    public PreferencesHelper(final Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void setLastCacheTime(final long lastCacheTime) {
        preferences.edit().putLong(KEY_LAST_CACHE_TIME, lastCacheTime).commit();
    }

    public long getLastCacheTime() {
        return preferences.getLong(KEY_LAST_CACHE_TIME, 0);
    }
}
