package com.aseevei.teamworkprojects.cache.mapper;

/**
 * Interface for model mappers. It provides helper methods that facilitate
 * retrieving of models from outer data source layers.
 *
 * @param <T> the cached model input type
 * @param <V> the model return type
 */
public interface EntityMapper<T, V> {

    public V mapFromCached(T type);

    public T mapToCached(V type);
}
